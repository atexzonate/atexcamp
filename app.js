var express             = require("express"),
    app                 = express(),
    bodyParser          = require('body-parser'),
    mongoose            = require("mongoose"),
    passport            = require("passport"),
    LocalStrategy       = require("passport-local"),
    Comment             = require("./models/comments.js"),
    Campground          = require("./models/campgrounds.js"),
    User                = require("./models/users.js"),
    seedDB              = require("./seeds");
    // Campground  = require("./models/campgrounds.js");
    // Campground  = require("./models/campgrounds.js");

app.set("port", (process.env.PORT || 3000));
// mongoose.connect("mongodb://localhost/yelp_camp");
mongoose.connect("mongodb://atexadmin:Welcome1@ds121251.mlab.com:21251/tasker_app");
app.use(bodyParser.urlencoded({extended: true}));
app.set("view engine", "ejs");
app.use(express.static(__dirname + "/public"));
seedDB();

// pass current User to every Template
app.use(function(req, res, next) {
  res.locals.currentUser = req.user;
  next();
});

//passport Configuration
app.use(require("express-session")({
  secret: "atexzonate",
  resave: false,
  saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());
passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

//{name: "Granite Hills",image: "https://images.pexels.com/photos/5922/wood-holiday-vacation-vintage.jpg?auto=compress&cs=tinysrgb&h=350",}
// {name:"Mountain Goat's Rest" ,image: "https://images.pexels.com/photos/216676/pexels-photo-216676.jpeg?auto=compress&cs=tinysrgb&h=350" }
// {name:"Salmon Creek", "image" : "https://images.pexels.com/photos/699558/pexels-photo-699558.jpeg?auto=compress&cs=tinysrgb&h=350"}


//====================
// Authentication Routes
//====================

app.get("/register",function(req, res){
  res.render("register");
});

// Submit signup form
app.post("/register", function(req, res){
  var newUser = new User({username: req.body.username});
  User.register(newUser, req.body.password, function(err, user) {
    if (err) {
      console.log(err);
      return res.render("register");
    }
    passport.authenticate("local")(req, res, function() {
      res.redirect("/campgrounds");
    })
  })
});

// Login
app.get("/login", function(req, res){
  res.render("login");
});

function isLoggedIn(req, res, next){
  if (req.isAuthenticated()) {
    return next();
  }
  res.redirect("/login");
};

app.post("/login", passport.authenticate("local",
{
  successRedirect: "/campgrounds",
  failureRedirect: "/login"
}), function(req, res){
});

// Logout
app.get("/logout", function(req, res){
  req.logout();
  res.redirect("/campgrounds")
});


app.get("/", function(req, res){
  res.render("homepage");
});

app.get("/campgrounds", function(req, res){
  console.log(req.user)
  //Get All campgrounds from DB
  Campground.find({}, function(err, allcampgrounds) {
    if (err) {
      console.log(err);
    }else {
      res.render("campgrounds/index", {campgrounds:allcampgrounds});
    }
  });
});

app.post("/campgrounds", function(req, res){
  //Get data from form and add to campgrounds array
  var name            = req.body.name,
      image           = req.body.image,
      description     = req.body.description,
      newCampgrounds  = {name: name, image: image, description: description};
  // Create newly added campground to DB
  Campground.create(newCampgrounds, function(err, newlyCreated) {
    if (err) {
      console.log(err);
    }else {
      //redirect back to Campgrounds page
      res.redirect("campgrounds/campgrounds");
    }
  });
});

app.get("/campgrounds/new",isLoggedIn, function(req, res){
  res.render("campgrounds/new");
});

app.get("/campgrounds/:id", function(req, res){
  //find the Campground with the provided ID
  Campground.findById(req.params.id).populate("comments").exec(function(err, foundCampground){
    if (err) {
      console.log(err);
    } else {
      //Render show Template
        res.render("campgrounds/show", {campground: foundCampground});
    }
  });
});

app.get("/campgrounds/:id/comments/new", isLoggedIn, function (req, res){
  // find by ID
  Campground.findById(req.params.id, function(err, campground){
    if (err) {
      console.log(err);
    }else {
      res.render("comments/new", {campground: campground});
    }
  })
});

app.post("/campgrounds/:id/comments", isLoggedIn, function(req, res) {
  //Lookup campground using ID`
  Campground.findById(req.params.id, function(err, campground ){
    if (err){
      console.log(err);
      res.redirect("campgrounds/campgrounds")
    }else {
        // Create a new Comment
        Comment.create(req.body.comment, function functionName(err, comment){
          if (err) {
            console.log(err);
          }else {
            campground.comments.push(comment)
            campground.save();
            res.redirect("/campgrounds/" + campground._id);
          }
        })
    }
  });
});


app.listen( app.get("port"), function(){
  console.log("Server Started on port", app.get("port"));
});
